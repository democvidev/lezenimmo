# LeZenImmobilier

- The project is a blog for a company specializing in collaborative urban planning and participatory housing.


## Requirements dev environment

- WAMP, PHP 7.4, Composer, Symfony CLI

- Check your configuration

```bash
symfony check:requirements
```

### Installation

```
git clone https://gitlab.com/democvidev/lezenimmo.git
cd lezenimmo
composer install 
symfony console d:d:c
symfony console d:m:m
symfony console server:start

```


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


## License

[MIT](https://choosealicense.com/licenses/mit/)